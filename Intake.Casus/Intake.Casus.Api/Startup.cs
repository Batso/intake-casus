using System;
using AutoMapper;
using Intake.Casus.Application.Extensions;
using Intake.Casus.Application.ToDoLists.Mappers;
using Intake.Casus.Application.ToDos.Mappers;
using Intake.Casus.Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Intake.Casus.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //if() {
                
            //        CurrentEnvironment.IsEnvironment("environmentname")
            //}


            services.AddControllers().AddNewtonsoftJson();

            services.AddAutoMapper(typeof(ToDoListMappingProfile), typeof(ToDoMappingProfile));

            services.ConfigureApplicationServices();
            services.AddEntityFrameworkCore(Configuration.GetConnectionString("DefaultConnection"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
