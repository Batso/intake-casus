﻿using Intake.Casus.Application.ToDos.Commands;
using Intake.Casus.Application.ToDos.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ToDosController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ToDosController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get to dos from to do list id
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ToDoQuery query)
        {
            var result = await _mediator.Send(query);

            return Ok(result);
        }

        /// <summary>
        /// Create to do list
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RegisterOrUpdateToDoCommand command, CancellationToken token)
        {
            var result = await _mediator.Send(command, token);

            return Ok(result);
        }

        /// <summary>
        /// Update to do list
        /// </summary>
        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody] RegisterOrUpdateToDoCommand command, CancellationToken token)
        {
            var result = await _mediator.Send(command, token);

            return Ok(result);
        }

        /// <summary>
        /// Delete to do list
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id, CancellationToken token)
        {
            var result = await _mediator.Send(new DeleteToDoCommand { Id = id }, token);

            return Ok(result);
        }
    }
}
