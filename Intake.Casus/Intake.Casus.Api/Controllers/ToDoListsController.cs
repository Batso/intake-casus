﻿using Intake.Casus.Application.ToDoLists.Commands;
using Intake.Casus.Application.ToDoLists.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ToDoListsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ToDoListsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get to do lists from user id
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ToDoListQuery query, CancellationToken token)
        {
            var result = await _mediator.Send(query, token);
            return Ok(result);
        }

        /// <summary>
        /// Create to do list
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RegisterOrUpdateToDoListCommand command, CancellationToken token)
        {
            var result = await _mediator.Send(command, token);

            return Ok(result);
        }

        /// <summary>
        /// Update to do list
        /// </summary>
        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody] RegisterOrUpdateToDoListCommand command, CancellationToken token)
        {
            var result = await _mediator.Send(command, token);

            return Ok(result);
        }

        /// <summary>
        /// Delete to do list
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id, CancellationToken token)
        {
            var result = await _mediator.Send(new DeleteToDoListCommand { Id = id }, token);

            return Ok(result);
        }
    }
}
