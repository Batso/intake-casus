﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xunit;

namespace Intake.Casus.Application.Tests
{
    public class UnitTests
    {

        public UnitTests()
        {

        }

        [Fact]
        public void When_Given_Then()
        {
            //Assign
            var number = 15;

            //Act
            fizzBuzz(number);

            //Assert
            Assert.NotNull(null);
        }

        [Theory]
        [InlineData(10000000, -1000000, -1, 24)]
        [InlineData(-5, -1, -3, -20)]
        [InlineData(5, 9, 6, 56)]
        [InlineData(-1, 0, 5, 34)]
        public void SequenceSum(int from, int to, int then, long expected)
        {
            long result = getSequenceSum(from, to, then);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void Arrival()
        {

        }

        //Get the sequence of the from and to in an int IEnumerable
        private static long getSumOfSequence(int from, int to, int offset = 1)
        {
            var counter = getCounter(from, to);

            var sequence = Enumerable.Range(from, counter + offset);

            if (offset == 0)
                return sequence.Reverse().Sum(x => (long)x);

            return sequence.Sum(x => (long)x);
        }

        //Get the counter for the range of the from and to
        private static int getCounter(int from, int to)
        {
            return Math.Abs(from - to);
        }

        private static long getSequenceSum(int from, int to, int then)
        {
            var fromToSequenceSum = getSumOfSequence(from, to);
            var toThenSequenceSum = getSumOfSequence(then, to, 0);

            return fromToSequenceSum + toThenSequenceSum;
        }

        private static bool IsFizzBuzz(int number)
        {
            return IsFizz(number) && IsBuzz(number);
        }

        private static bool IsFizz(int number)
        {
            return number % 3 == 0;
        }

        private static bool IsBuzz(int number)
        {
            return number % 5 == 0;
        }

        private static void fizzBuzz(int total)
        {
            var printFizzBuzzOutput = "FizzBuzz";
            var printFizzOutput = "Fizz";
            var printBuzzOutput = "Buzz";

            for (int current = 1; current <= total; current++)
            {
                string output =
                    IsFizzBuzz(current) ? printFizzBuzzOutput :
                    IsFizz(current) ? printFizzOutput :
                    IsBuzz(current) ? printBuzzOutput :
                    current.ToString();
                Console.WriteLine(output);
            }
        }
    }
}
