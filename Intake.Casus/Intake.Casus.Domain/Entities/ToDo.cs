﻿using Intake.Casus.Domain.Interfaces;

namespace Intake.Casus.Domain.Entities
{
    public class ToDo : IIdentity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDone { get; set; }

        public int ToDoListId { get; set; }
        public ToDoList ToDoList { get; set; }
    }
}
