﻿using Intake.Casus.Domain.Interfaces;
using System.Collections.Generic;

namespace Intake.Casus.Domain.Entities
{
    public class ToDoList : IIdentity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<ToDo> ToDos { get; set; }
    }
}
