﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Domain.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class, IIdentity
    {
        IQueryable<TEntity> AsQueryable();
        Task<List<TEntity>> Get(CancellationToken cancellationToken, bool noTracking = false);
        Task<TEntity> GetById(int id, CancellationToken cancellationToken, bool noTracking = false);
        Task<int> Create(TEntity entity, CancellationToken cancellationToken);
        Task<int> UpdateAsync(TEntity entity, CancellationToken cancellationToken);
        Task<int> DeleteAsync(int id, CancellationToken cancellationToken);
    }
}
