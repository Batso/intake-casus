﻿namespace Intake.Casus.Domain.Interfaces
{
    public interface IIdentity
    {
        int Id { get; set; }
    }
}
