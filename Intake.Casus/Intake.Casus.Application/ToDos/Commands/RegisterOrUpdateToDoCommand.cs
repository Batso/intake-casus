﻿using AutoMapper;
using Intake.Casus.Application.ToDos.Models;
using Intake.Casus.Domain.Entities;
using Intake.Casus.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Application.ToDos.Commands
{
    public class RegisterOrUpdateToDoCommand : ToDoModel, IRequest<int>
    {
    }

    public class RegisterOrUpdateToDoCommandHandler : IRequestHandler<RegisterOrUpdateToDoCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<ToDo> _repository;

        public RegisterOrUpdateToDoCommandHandler(IMapper mapper, IRepository<ToDo> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<int> Handle(RegisterOrUpdateToDoCommand request, CancellationToken cancellationToken)
        {
            var dbEntity = _mapper.Map<ToDo>(request);

            var result = await _repository.UpdateAsync(dbEntity, cancellationToken);

            return result;
        }
    }
}
