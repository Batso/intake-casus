﻿using Intake.Casus.Domain.Entities;
using Intake.Casus.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Application.ToDos.Commands
{
    public class DeleteToDoCommand : IRequest<int>
    {
        public int Id { get; set; }
    }

    public class DeleteToDoCommandHandler : IRequestHandler<DeleteToDoCommand, int>
    {
        private readonly IRepository<ToDo> _repository;

        public DeleteToDoCommandHandler(IRepository<ToDo> repository)
        {
            _repository = repository;
        }

        public async Task<int> Handle(DeleteToDoCommand request, CancellationToken cancellationToken)
        {
            var result = await _repository.DeleteAsync(request.Id, cancellationToken);

            return result;
        }
    }
}
