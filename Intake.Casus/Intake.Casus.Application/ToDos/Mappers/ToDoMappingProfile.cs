﻿using AutoMapper;
using Intake.Casus.Application.ToDos.Commands;
using Intake.Casus.Application.ToDos.Models;
using Intake.Casus.Domain.Entities;

namespace Intake.Casus.Application.ToDos.Mappers
{
    public class ToDoMappingProfile : Profile
    {
        public ToDoMappingProfile()
        {
            CreateMap<ToDo, ToDoModel>();
            CreateMap<RegisterOrUpdateToDoCommand, ToDo>();
        }
    }
}
