﻿using AutoMapper;
using Intake.Casus.Application.ToDos.Models;
using Intake.Casus.Domain.Entities;
using Intake.Casus.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Application.ToDos.Queries
{
    public class ToDoQuery : IRequest<IEnumerable<ToDoModel>>
    {
        public int ToDoListId { get; set; }
    }

    public class ToDoQueryHandler : IRequestHandler<ToDoQuery, IEnumerable<ToDoModel>>
    {
        private readonly IRepository<ToDo> _repository;
        private readonly IMapper _mapper;


        public ToDoQueryHandler(IMapper mapper, IRepository<ToDo> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<IEnumerable<ToDoModel>> Handle(ToDoQuery query, CancellationToken cancellationToken)
        {
            var dbEntities = await _repository.AsQueryable()
                                        .Where(_ => _.ToDoListId == query.ToDoListId)
                                        .ToListAsync(cancellationToken);

            var mapped = _mapper.Map<IEnumerable<ToDoModel>>(dbEntities);

            return mapped;
        }
    }
}
