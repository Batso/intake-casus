﻿
namespace Intake.Casus.Application.ToDoLists.Models
{
    public class ToDoListModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
