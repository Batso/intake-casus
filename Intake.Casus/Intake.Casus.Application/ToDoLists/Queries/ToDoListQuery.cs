﻿using AutoMapper;
using Intake.Casus.Application.ToDoLists.Models;
using Intake.Casus.Domain.Entities;
using Intake.Casus.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Application.ToDoLists.Queries
{
    public class ToDoListQuery : IRequest<IEnumerable<ToDoListModel>>
    {
    }

    public class ToDoListQueryHandler : IRequestHandler<ToDoListQuery, IEnumerable<ToDoListModel>>
    {
        private readonly IRepository<ToDoList> _repository;
        private readonly IMapper _mapper;


        public ToDoListQueryHandler(IMapper mapper, IRepository<ToDoList> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<IEnumerable<ToDoListModel>> Handle(ToDoListQuery query, CancellationToken cancellationToken)
        {
            var dbEntities = await _repository.Get(cancellationToken, true);

            var mapped = _mapper.Map<IEnumerable<ToDoListModel>>(dbEntities);

            return mapped;
        }
    }
}
