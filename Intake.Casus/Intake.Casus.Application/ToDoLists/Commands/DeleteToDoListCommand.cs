﻿using Intake.Casus.Domain.Entities;
using Intake.Casus.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Application.ToDoLists.Commands
{
    public class DeleteToDoListCommand : IRequest<int>
    {
        public int Id { get; set; }
    }

    public class DeleteToDoListCommandHandler : IRequestHandler<DeleteToDoListCommand, int>
    {
        private readonly IRepository<ToDoList> _repository;

        public DeleteToDoListCommandHandler(IRepository<ToDoList> repository)
        {
            _repository = repository;
        }

        public async Task<int> Handle(DeleteToDoListCommand request, CancellationToken cancellationToken)
        {
            var result = await _repository.DeleteAsync(request.Id, cancellationToken);

            return result;
        }
    }
}
