﻿using AutoMapper;
using Intake.Casus.Application.ToDoLists.Models;
using Intake.Casus.Domain.Entities;
using Intake.Casus.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Application.ToDoLists.Commands
{
    public class RegisterOrUpdateToDoListCommand : ToDoListModel, IRequest<int>
    {
    }

    public class RegisterOrUpdateToDoListCommandHandler : IRequestHandler<RegisterOrUpdateToDoListCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<ToDoList> _repository;

        public RegisterOrUpdateToDoListCommandHandler(IMapper mapper, IRepository<ToDoList> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<int> Handle(RegisterOrUpdateToDoListCommand request, CancellationToken cancellationToken)
        {
            var dbEntity = _mapper.Map<ToDoList>(request);

            var result = await _repository.UpdateAsync(dbEntity, cancellationToken);

            return result;
        }
    }
}
