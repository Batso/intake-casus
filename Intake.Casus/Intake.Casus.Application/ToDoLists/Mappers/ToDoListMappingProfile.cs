﻿using AutoMapper;
using Intake.Casus.Application.ToDoLists.Commands;
using Intake.Casus.Application.ToDoLists.Models;
using Intake.Casus.Domain.Entities;

namespace Intake.Casus.Application.ToDoLists.Mappers
{
    public class ToDoListMappingProfile : Profile
    {
        public ToDoListMappingProfile()
        {
            CreateMap<ToDoList, ToDoListModel>();
            CreateMap<RegisterOrUpdateToDoListCommand, ToDoList>();
        }
    }
}
