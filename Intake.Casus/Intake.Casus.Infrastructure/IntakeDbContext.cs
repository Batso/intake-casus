﻿using Intake.Casus.Domain.Entities;
using Intake.Casus.Infrastructure.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Intake.Casus.Infrastructure
{
    public class IntakeDbContext : DbContext
    {
        public IntakeDbContext(DbContextOptions<IntakeDbContext> options) : base(options) { }

        public DbSet<ToDoList> ToDoLists { get; set; }
        public DbSet<ToDo> ToDos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ToDoListTypeConfigurations());
        }
    }
}
