﻿using Intake.Casus.Domain.Interfaces;
using Intake.Casus.Infrastructure.Data;
using Intake.Casus.Infrastructure.Data.Repositories;
using Intake.Casus.Infrastructure.Data.Seeds;
using Intake.Casus.Infrastructure.Helpers;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Intake.Casus.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEntityFrameworkCore(this IServiceCollection services, string connectionString)
        {
            services.AddDbContextPool<IntakeDbContext>((provider, options)
                 => DbContextOptionsHelper.ConfigureOptionsBuilder(options, connectionString));

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            return services;
        }

        public static void InitializeDatabase(this IServiceProvider serviceProvider, Action<IntakeDbContext> seedAction)
        {
            var dbContext = serviceProvider.GetRequiredService<IntakeDbContext>();
            new DbMigrator<IntakeDbContext>(dbContext).CreateOrMigrate(seedAction);
        }

        public static void SeedDatabase(this IServiceProvider services)
        {
            try
            {
                services.InitializeDatabase((context) => new DevDatabaseSeed(context).Run());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
