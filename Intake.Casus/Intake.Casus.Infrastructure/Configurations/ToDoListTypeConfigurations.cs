﻿using Intake.Casus.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Intake.Casus.Infrastructure.Configurations
{
    public class ToDoListTypeConfigurations : IEntityTypeConfiguration<ToDoList>
    {
        public void Configure(EntityTypeBuilder<ToDoList> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.Id).ValueGeneratedOnAdd();

            builder
               .HasMany(_ => _.ToDos)
               .WithOne(_ => _.ToDoList)
               .HasForeignKey(_ => _.ToDoListId);
        }
    }

    public class ToDoTypeConfigurations : IEntityTypeConfiguration<ToDo>
    {
        public void Configure(EntityTypeBuilder<ToDo> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.Id).ValueGeneratedOnAdd();

            builder
                .HasOne(_ => _.ToDoList)
                .WithMany(_ => _.ToDos)
                .HasForeignKey(_ => _.ToDoListId);
        }
    }
}
