﻿using Intake.Casus.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Intake.Casus.Infrastructure.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity>
       where TEntity : class, IIdentity
    {
        protected readonly IntakeDbContext Context;

        public Repository(IntakeDbContext context)
        {
            Context = context;
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return Context.Set<TEntity>().AsQueryable();
        }

        private IQueryable<TEntity> Get(bool noTracking)
        {
            var query = Context.Set<TEntity>();

            if (noTracking)
                query.AsNoTracking();

            return query;
        }

        public virtual Task<List<TEntity>> Get(CancellationToken cancellationToken, bool noTracking = false)
        {
            return Get(noTracking).ToListAsync(cancellationToken);
        }

        public virtual Task<TEntity> GetById(int id, CancellationToken cancellationToken, bool noTracking = false)
        {
            return AsQueryable().FirstOrDefaultAsync(c => c.Id.Equals(id), cancellationToken);
        }

        public virtual Task<int> Create(TEntity entity, CancellationToken cancellationToken)
        {
            Context.Set<TEntity>().Add(entity);
            return Context.SaveChangesAsync(cancellationToken);
        }

        public virtual Task<int> UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            Context.Set<TEntity>().Update(entity);
            return Context.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task<int> DeleteAsync(int id, CancellationToken cancellationToken)
        {
            var entity = await GetById(id, cancellationToken);
            Context.Set<TEntity>().Remove(entity);
            return await Context.SaveChangesAsync(cancellationToken);
        }
    }
}
