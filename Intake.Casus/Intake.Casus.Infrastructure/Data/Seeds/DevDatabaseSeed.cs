﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intake.Casus.Infrastructure.Data.Seeds
{
    public class DevDatabaseSeed
    {
        private readonly IntakeDbContext _dbContext;

        public DevDatabaseSeed(IntakeDbContext context)
        {
            _dbContext = context;
        }

        public void Run()
        {
        }
    }
}
