﻿using Intake.Casus.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intake.Casus.Infrastructure.Data.Seeds
{
    public class TestDatabaseSeed
    {
        private readonly IntakeDbContext _dbContext;

        public TestDatabaseSeed(IntakeDbContext context)
        {
            _dbContext = context;
        }

        public void Run()
        {
            var todoList = new ToDoList()
            {
                Name = "Intake todo"
            };

            _dbContext.ToDoLists.Add(todoList);
            _dbContext.SaveChanges();
        }
    }
}
