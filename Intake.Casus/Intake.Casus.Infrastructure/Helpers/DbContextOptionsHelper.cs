﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace Intake.Casus.Infrastructure.Helpers
{
    internal static class DbContextOptionsHelper
    {
        private static readonly InMemoryDatabaseRoot _inMemoryDatabaseRoot = new InMemoryDatabaseRoot();

        public static void ConfigureOptionsBuilder(DbContextOptionsBuilder builder, string connectionString)
        {
            if (connectionString.IndexOf("inMemory", StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                // don't raise the error warning us that the in memory db doesn't support transactions
                builder.ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning));

                var dbName = "EFProviders.InMemory";

                builder.UseInMemoryDatabase(dbName, _inMemoryDatabaseRoot);
            }
            else
            {
                builder.UseSqlServer(connectionString);
            }
        }
    }
}
